package com.stefanamladenovic.diary;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.regex.Pattern;

public class ActivitySignUp extends AppCompatActivity {


    private static final int RC_SIGN_IN = 0;
    private EditText email;
    private EditText password;
    SignInButton signInButtonGoogle;
    private CheckBox rememberMe;


    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    Button buttonLogin;
    Button buttonSignIn;

    FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;




//    @Override
//    protected void onStart() {
//        super.onStart();
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        rememberMe = findViewById(R.id.remember_me);
        rememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            }
        });

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            Intent i = new Intent(ActivitySignUp.this, ActivityListOfNotes.class);
            startActivity(i);
            return;
        }



        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        signInButtonGoogle = findViewById(R.id.sign_in_button_google);


       // findViews();

        buttonLogin = findViewById(R.id.button_login);
        buttonSignIn = findViewById(R.id.button_signin);


        //ovo treba da proveri mail i pass
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkIfPassIsConfirmed(password) == true && checkEmail(email) == true) {


                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                       @Override
                                                       public void onComplete(@NonNull Task<AuthResult> task) {
                                                           if (task.isSuccessful()) {
                                                               Toast.makeText(ActivitySignUp.this, "You are successfully signed in!",
                                                                       Toast.LENGTH_SHORT).show();
                                                               // Sign in success, update UI with the signed-in user's information
                                                               Intent i = new Intent(ActivitySignUp.this, ActivityListOfNotes.class);
                                                               startActivity(i);


                                                           } else {
                                                               // If sign in fails, display a message to the user.
                                                               Toast.makeText(ActivitySignUp.this, task.getException().getLocalizedMessage(),
                                                                       Toast.LENGTH_SHORT).show();
                                                           }
                                                       }
                                                   }
                            );

                } else {
                    Toast.makeText(ActivitySignUp.this, "Invalid mail or password", Toast.LENGTH_SHORT).show();
                    cancelIfNotValid(email, password);
                }
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(email.getText().toString()) && !TextUtils.isEmpty(password.getText().toString()))
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            Intent i = new Intent(ActivitySignUp.this, ActivityListOfNotes.class);
                            startActivity(i);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(ActivitySignUp.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            cancelIfNotValid(email, password);
                        }
                    });

            }
        });

        signInButtonGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.sign_in_button_google:
                        signIn();
                        break;
                    // ...
                }
            }
        });
    }


    //ovo je metoda koja proveraca pass

    private boolean checkIfPassIsConfirmed(EditText password) {

        if (password.getText().toString().length() < 5) {
            Toast.makeText(ActivitySignUp.this, "Password doesn't match", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }


    //ovo je metoda koja proverava mejl

    private boolean checkEmail(EditText email) {

        return EMAIL_ADDRESS_PATTERN.matcher(email.getText().toString()).matches();
    }


    //ove brise napisano ako nije validno

    private void cancelIfNotValid(EditText email, EditText firstPass) {

        email.setText("");
        firstPass.setText("");
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        Log.w("Stefana", "Google sign in started");

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.w("Stefana", "Google sign result");
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
                //ako se uspesno ulogovao da udje u aplikaciju
                Intent intent = new Intent(ActivitySignUp.this, ActivityListOfNotes.class);
                startActivity(intent);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("Stefana", "Google sign in failed", e);
                // ...
            }
        }
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("Login", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            firebaseAuthWithGoogle(acct);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("Stefana", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Stefana", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Stefana", "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.activity_sign_up), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //  updateUI(null);
                        }

                        // ...
                    }
                });
    }


}
