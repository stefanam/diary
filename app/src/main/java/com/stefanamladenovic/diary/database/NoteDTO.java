package com.stefanamladenovic.diary.database;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NoteDTO implements Parcelable{

    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy.");
    private String text;
    private String emotion;
    private String date;
    private String userId;//ID USERA KOJI JE NAPRAVIO NOTE
    private String id;//ID NOTE-A
    private String image;

    public NoteDTO(String text, String emotion, String date, String userId, String id) {
        this.text = text;
        this.emotion = emotion;
        this.date = date;
        this.userId = userId;
        this.id = id;
    }


    //parcelable implementation

    protected NoteDTO(Parcel in) {
        text = in.readString();
        emotion = in.readString();
        date = in.readString();
        image = in.readString();
    }

    public static final Creator<NoteDTO> CREATOR = new Creator<NoteDTO>() {
        @Override
        public NoteDTO createFromParcel(Parcel in) {
            return new NoteDTO(in);
        }

        @Override
        public NoteDTO[] newArray(int size) {
            return new NoteDTO[size];
        }
    };

    // end of parcelable implementation



    public static SimpleDateFormat getFormat() {
        return format;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public NoteDTO() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object[] createRowData() {
        return new String[]{format.format(new Date(date)), emotion, text};
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static NoteDTO convertFrom(ResultSet resultSet) throws SQLException {
        NoteDTO noteDTO = new NoteDTO();
        noteDTO.setId(resultSet.getString(1));
        noteDTO.setUserId(resultSet.getString(2));
        noteDTO.setDate(resultSet.getString(3));
        noteDTO.setEmotion(resultSet.getString(4));
        noteDTO.setText(resultSet.getString(5));
        return noteDTO;
    }

    @Override
    public String toString() {
        return "Created: " + format.format(new Date(date)) +
                "Emotion: " + emotion + "\n\n" + text;
    }

    public boolean contains(String query) {

        return text.toLowerCase().contains(query);
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }


   // parcelable methods nedded to be implemented

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(text);
        parcel.writeString(emotion);
        parcel.writeString(date);
        parcel.writeString(image);
    }
}
