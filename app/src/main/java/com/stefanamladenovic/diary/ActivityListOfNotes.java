package com.stefanamladenovic.diary;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.usage.UsageEvents;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.stefanamladenovic.diary.adapter.CustomAdapter;
import com.stefanamladenovic.diary.adapter.NoteDetailsActivity;
import com.stefanamladenovic.diary.adapter.RecyclerItemTouchHelper;
import com.stefanamladenovic.diary.adapter.RecyclerViewClickListener;
import com.stefanamladenovic.diary.adapter.RowViewHolder;
import com.stefanamladenovic.diary.database.NoteDTO;
import com.stefanamladenovic.diary.datepicker.DatePickerFragment;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.events.Event;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ActivityListOfNotes extends AppCompatActivity
        implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private FloatingActionButton addNewNote;
    private RecyclerView list;
    private List<NoteDTO> originalNotes;
    private CustomAdapter adapter;
    private Toolbar toolbar;
    private DatabaseReference databaseReference;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd LLLL yyyy");
    private String currentDate;
    private SwipeRefreshLayout swiper;
    private CaldroidFragment caldroidFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_notes);

        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        addNewNote = findViewById(R.id.addNote);

        addNewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent addNewNoteIntent = new Intent(ActivityListOfNotes.this, NewNoteActivity.class);
                startActivity(addNewNoteIntent);
            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        list = findViewById(R.id.recycler_view);
        list.setLayoutManager(llm);
        list.setItemAnimator(new DefaultItemAnimator());
        list.addItemDecoration(new SpacesItemDecoration(12));


        swiper = ((SwipeRefreshLayout) findViewById(R.id.swiperefresh));

        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(currentDate);
            }
        });

        FirebaseAuth.getInstance().addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() != null){

                    databaseReference = FirebaseDatabase.getInstance().getReference()
                            .child(firebaseAuth.getCurrentUser().getUid());
                    //initial refresh
                    refresh(dateFormat.format(Calendar.getInstance().getTime()));
                }
            }
        });

    }


    private void refresh(final String date) {

        this.currentDate = date;

        ((CollapsingToolbarLayout) findViewById(R.id.main_collapsing)).setTitle(date);

        databaseReference.child(date).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                originalNotes = new ArrayList<>();

                for (DataSnapshot noteSnapshot : dataSnapshot.getChildren()) {
                    NoteDTO note = noteSnapshot.getValue(NoteDTO.class);//json u objekat DTO
                    note.setId(dataSnapshot.getKey());
                    originalNotes.add(note);
                }

                list.setAdapter(adapter = new CustomAdapter(originalNotes,
                        new RecyclerViewClickListener() {
                            @Override
                            public void onClick(View view, int position) {
//                                Intent intent = new Intent(ActivityListOfNotes.this, NoteDetailsActivity.class);
//                                intent.putExtra("note", originalNotes.get(position));
//                                startActivity(intent);

                            }
                        }));

                ItemTouchHelper.SimpleCallback itemTouchHelperCallback =
                        new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, ActivityListOfNotes.this);
                new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        swiper.setRefreshing(false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_of_notes, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.logout) {
            logout();
        } else if (item.getItemId() == R.id.calendar) {

            //DialogFragment datePicker = new DatePickerFragment();
            //datePicker.show(getFragmentManager(), "date picker");

            showCaldroidFragment();
        }

        return super.onOptionsItemSelected(item);
    }

    public void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(ActivityListOfNotes.this, ActivitySignUp.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction, final int position) {
        if (viewHolder instanceof RowViewHolder) {
            // backup of removed item for undo purpose

            //First show me Alert dialog to confirm deleting
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm");
            builder.setMessage("Are you sure that you want to delete this note?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Deleting note if yes
                    final NoteDTO deletedItem = originalNotes.get(viewHolder.getAdapterPosition());
                    FirebaseDatabase
                            .getInstance()
                            .getReference()
                            .child(FirebaseAuth
                                    .getInstance()
                                    .getCurrentUser()
                                    .getUid())
                            .child(deletedItem.getDate())
                            .child(deletedItem.getId())
                            .removeValue()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    adapter.removeItem(position);
                                    saveNotes();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(ActivityListOfNotes.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });

                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    refresh(dateFormat.format(Calendar.getInstance().getTime()));
                }
            });
            builder.show();
        }
    }


    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;

            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = space;
            }
        }
    }

    public void saveNotes() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(dateFormat.format(Calendar.getInstance().getTime()));
        ref.setValue(originalNotes);
        ref.push();
    }

    private void showCaldroidFragment() {
        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                HashMap<String, Object> value = (HashMap<String, Object>) dataSnapshot.getValue();

                SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
                //U Firebase-u su datumi smesteni kao String
                //Ovim konvertujemo taj String iz Firebase-a u realan datum
                if (value != null){
                Iterator<String> iterator = value.keySet().iterator(); //dohvatamo kljuceve, to su datumi
                Map<Date, Drawable> map = new HashMap<>(); //pravimo novu mapu, jer on ocekuje datum i dravable
                ColorDrawable green = new ColorDrawable(Color.GREEN);
                while(iterator.hasNext()){
                    try {
                        map.put(format.parse(iterator.next()), green);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                caldroidFragment.setBackgroundDrawableForDates(map);
                caldroidFragment.refreshView();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        final Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
        caldroidFragment.setMaxDate(Calendar.getInstance().getTime());
        caldroidFragment.setArguments(args);
        caldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {

                SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
                String month_name = month_date.format(date);

                SimpleDateFormat year_format = new SimpleDateFormat("yyyy");
                String year = year_format.format(date);

                SimpleDateFormat day_format = new SimpleDateFormat("dd");
                int day = Integer.parseInt(day_format.format(date));

                // int properMonth = month + 1;

                refresh((day < 10 ? "0" + day : day) + " " + month_name + " " + year);
                caldroidFragment.dismiss();
            }
        });
        caldroidFragment.show(getSupportFragmentManager(),"TAG");
    }

}
