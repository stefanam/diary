package com.stefanamladenovic.diary;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.stefanamladenovic.diary.adapter.EmotionsAdapter;
import com.stefanamladenovic.diary.database.NoteDTO;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class NewNoteActivity extends AppCompatActivity {

    private RecyclerView emotions;
    private EditText noteText;
    private String emotion = "Awesome";
    private ImageView image;
    private FloatingActionButton addImage;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd LLLL yyyy");
    private Image selectedImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_layout);


        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        emotions = findViewById(R.id.emotions);
        emotions.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        emotions.setAdapter(new EmotionsAdapter(Arrays.asList(getResources().getStringArray(R.array.emotion_array)), new EmotionsAdapter.EmotionSelectedListener() {
            @Override
            public void emotionSelected(String emotion) {
                NewNoteActivity.this.emotion = emotion;
            }
        }));


        noteText = findViewById(R.id.note);
        image = findViewById(R.id.imageView);
        addImage = findViewById(R.id.addPicture);
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

    }

    private void showFileChooser() {
        ImagePicker.create(this)
                .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .single() // single mode
                .start(); // start image picker activity with request code
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_new_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            save();
        } else if (item.getItemId() == R.id.logout) {
            logout();
        }
        return super.onOptionsItemSelected(item);
    }

    public void save() {

        final NoteDTO note = new NoteDTO();
        note.setText(noteText.getText().toString());
        note.setEmotion(emotion);
        note.setDate(dateFormat.format(Calendar.getInstance().getTime()));
        note.setUserId(FirebaseAuth.getInstance().getCurrentUser().getUid());

        if (selectedImage != null) {
            //uploadovati sliku

            final String firebaseImagePath = "images/" + UUID.randomUUID().toString();

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();

            FirebaseStorage
                    .getInstance()
                    .getReference()
                    .child(firebaseImagePath)
                    .putFile(Uri.fromFile(new File(selectedImage.getPath())))
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            note.setImage(firebaseImagePath);
                            saveNote(note);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(NewNoteActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setProgress((int) progress);
                        }
                    });
        } else {
            saveNote(note);
        }
    }

    private void saveNote(NoteDTO note) {
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(note.getDate())
                .child(UUID.randomUUID().toString())
                .setValue(note)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(NewNoteActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(NewNoteActivity.this, ActivitySignUp.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            selectedImage = ImagePicker.getFirstImageOrNull(data);
            //path je najbitniji, uz pomoc fajla dobijamo URI a URI umemo da ucitamo

            //image.setImageURI(Uri.fromFile(new File(selectedImage.getPath())));

            //uz pomoc glidea ista stvar
            Glide.with(this)
                    .load(selectedImage.getPath())
                    .into(image);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
