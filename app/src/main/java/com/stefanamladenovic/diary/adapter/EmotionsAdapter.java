package com.stefanamladenovic.diary.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stefanamladenovic.diary.R;

import java.util.List;

public class EmotionsAdapter extends RecyclerView.Adapter<EmotionViewHolder> {

    private List<String> data;
    private EmotionSelectedListener listener;
    private int selectedEmotion = 0;

    public interface EmotionSelectedListener {

        void emotionSelected(String emotion);

    }

    public EmotionsAdapter(List<String> data, EmotionSelectedListener listener) {
        this.data = data;
        this.listener = listener;
    }

    @NonNull
    @Override
    public EmotionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EmotionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.emotion_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final EmotionViewHolder holder, final int position) {
        Context context = holder.imageView.getContext();
        holder.imageView.setImageResource(
                context.getResources().getIdentifier(
                        "ic_" + data.get(position).toLowerCase().replace(" ", "_") ,
                        "drawable", context.getPackageName()));
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedEmotion = position;
                notifyDataSetChanged();
                listener.emotionSelected(data.get(position));
            }
        });
        if(selectedEmotion == position){
            holder.itemView.setBackgroundResource(R.color.yellow700);
        }else{
            holder.itemView.setBackgroundResource(R.color.transparent);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
