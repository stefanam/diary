package com.stefanamladenovic.diary.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stefanamladenovic.diary.ActivityListOfNotes;
import com.stefanamladenovic.diary.R;
import com.stefanamladenovic.diary.database.NoteDTO;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class NoteDetailsActivity extends AppCompatActivity {

    Button okButton;
    TextView date;
    TextView emotion;
    TextView note;
    ImageView image;

    @Override
    protected void onResume() {
        super.onResume();

        NoteDTO noteDTO = getIntent().getParcelableExtra("note");
        date.setText(noteDTO.getDate().toString());
        emotion.setText(noteDTO.getEmotion().toString());
        note.setText(noteDTO.getText().toString());


        if (noteDTO.getImage() != null) {

            image.setVisibility(View.VISIBLE);

            StorageReference imageRef = FirebaseStorage.getInstance().getReference().child(noteDTO.getImage());

            Glide.with(
                    this
                    )
                    .load(imageRef)
                    .into(image);

        } else {
            image.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);

        date = findViewById(R.id.dateOfNote);
        emotion = findViewById(R.id.emotionForDay);
        note = findViewById(R.id.noteForDay);
        image = findViewById(R.id.imageViewForDay);
        okButton = findViewById(R.id.buttonOk);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        Intent i = new Intent(NoteDetailsActivity.this, ActivityListOfNotes.class);
                        startActivity(i);

            }
        });
    }
}