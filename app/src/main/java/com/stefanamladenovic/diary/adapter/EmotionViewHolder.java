package com.stefanamladenovic.diary.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

class EmotionViewHolder extends RecyclerView.ViewHolder{

    public ImageView imageView;

    public EmotionViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView;
    }
}
