package com.stefanamladenovic.diary.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stefanamladenovic.diary.R;

public class RowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public RecyclerViewClickListener mListener;
    public TextView date;
    public TextView emotion;
    public TextView textOfNote;
    public View foreground;
    public ImageView noteImage;
    public CardView emotionCard;
    public CardView dayCard;
    public CardView wholeCard;


    public RowViewHolder(View itemView, RecyclerViewClickListener listener) {
        super(itemView);
        mListener = listener;
        itemView.setOnClickListener(this);
        foreground = itemView.findViewById(R.id.foreground_view);

        emotion = itemView.findViewById(R.id.emotion);
        date = itemView.findViewById(R.id.day);
        textOfNote = itemView.findViewById(R.id.textOfNote);

        emotionCard = itemView.findViewById(R.id.cardEmotion);
        dayCard = itemView.findViewById(R.id.cardDay);
        wholeCard = itemView.findViewById(R.id.wholeCard);
        noteImage = itemView.findViewById(R.id.note_image);

    }

    @Override
    public void onClick(View view) {
        mListener.onClick(view, getAdapterPosition());
    }
}
