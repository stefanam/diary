package com.stefanamladenovic.diary.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.stefanamladenovic.diary.R;
import com.stefanamladenovic.diary.database.NoteDTO;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<RowViewHolder> {

    private List<NoteDTO> data;
    private RecyclerViewClickListener mListener;


    public CustomAdapter(List<NoteDTO> data, RecyclerViewClickListener listener) {
        this.data = data;
        mListener = listener;

    }


    @NonNull
    @Override
    public RowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        View v = LayoutInflater.from(context).inflate(R.layout.row_item, parent, false);

        return new RowViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RowViewHolder holder, int position) {
        NoteDTO item = data.get(position);
        holder.date.setText(item.getDate());
        holder.emotion.setText(item.getEmotion());
        holder.textOfNote.setText(item.getText());

        switch (item.getEmotion()) {
            case "Awesome":
                holder.emotionCard.setBackgroundColor(
                        ContextCompat.getColor(holder.emotionCard.getContext(), R.color.purple700));
                holder.dayCard.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.purple700));
                holder.foreground.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.purple100));
                holder.emotion.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.purple50));
                holder.date.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.purple50));
                break;
            case "Great":
                holder.emotionCard.setBackgroundColor(
                        ContextCompat.getColor(holder.emotionCard.getContext(), R.color.pink700));
                holder.dayCard.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.pink700));
                holder.foreground.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.pink100));
                holder.emotion.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.pink50));
                holder.date.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.pink50));

                break;
            case "Fine":
                holder.emotionCard.setBackgroundColor(
                        ContextCompat.getColor(holder.emotionCard.getContext(), R.color.green700));
                holder.dayCard.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.green700));
                holder.foreground.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.green100));
                holder.emotion.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.green50));
                holder.date.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.green50));
                break;
            case "Could be better":
                holder.emotionCard.setBackgroundColor(
                        ContextCompat.getColor(holder.emotionCard.getContext(), R.color.yellow700));
                holder.dayCard.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.yellow700));
                holder.foreground.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.yellow200));
                holder.emotion.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.yellow50));
                holder.date.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.yellow50));
                break;
            case "Terrible":
                holder.emotionCard.setBackgroundColor(
                        ContextCompat.getColor(holder.emotionCard.getContext(), R.color.brown600));
                holder.dayCard.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.brown600));
                holder.foreground.setBackgroundColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.brown200));
                holder.emotion.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.brown50));
                holder.date.setTextColor(ContextCompat.getColor(holder.emotionCard.getContext(), R.color.brown50));
                break;
        }


        if (item.getImage() != null) {
            holder.noteImage.setVisibility(View.VISIBLE);

            StorageReference imageRef = FirebaseStorage.getInstance().getReference().child(item.getImage());

            Glide.with(holder.itemView.getContext())
                    .load(imageRef)
                    .into(holder.noteImage);

        } else {
            holder.noteImage.setVisibility(View.GONE);
        }


    }


    @Override
    public int getItemCount() {

        return data.size();
    }

    public void removeItem(int position) {

        data.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(NoteDTO item, int position) {
        data.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }
}
